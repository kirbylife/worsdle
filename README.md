## Worsdle

A simple [Wordle](https://es.wikipedia.org/wiki/Wordle) clone written in Rust and Yem with spanish words.

### Dependencies to build

- [Trunk](https://trunkrs.dev/)

### How to build to production

1. `cargo install --locked trunk`
2. `git clone https://git.kirbylife.dev/kirbylife/worsdle`
3. `cd worsdle`
4. `trunk build --release`
5. The build should be in the `dist/` folder.

### Run a dev server

1. `trunk serve`

### Contribute

If you want to add a new word to the list, you can pass it to me through any social network.
