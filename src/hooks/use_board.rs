use crate::consts::{AttemptField, Attempts, GameResult, Key, VirtualKey, MAX_ATTEMPTS, Status};
use crate::services::words::{get_word_of_the_day, WORDS};
use crate::utils::{evaluate_status, new_empty_virtual_keyboard};
use std::cell::RefCell;
use std::rc::Rc;
use yew::prelude::*;

type AttemptRow = Attempts;

#[derive(PartialEq, Clone)]
pub struct UseBoardHandle {
    pub current_attempt: UseStateHandle<String>,
    pub attempts: UseStateHandle<AttemptRow>,
    pub virtual_keyboard: UseStateHandle<Vec<VirtualKey>>,
    pub result: UseStateHandle<Option<GameResult>>,
    pub attempt_index: Rc<RefCell<usize>>,
    answer: Rc<String>,
}

impl UseBoardHandle {
    pub fn send_key(&self, k: Key) {
        let current_attempt_len = self.current_attempt.chars().count();

        if self.result.is_some() {
            return;
        }

        match k {
            Key::CharKey(c) => {
                if current_attempt_len >= 5 {
                    return;
                }

                let mut new_attempt = (*self.current_attempt).clone();
                new_attempt.push(c);
                self.current_attempt.set(new_attempt);
            }
            Key::Backspace => {
                if current_attempt_len == 0 {
                    return;
                }

                let mut new_attempt = (*self.current_attempt).clone();
                // This unwrap is safe because the potential failura it's already covered
                new_attempt.pop().unwrap();
                self.current_attempt.set(new_attempt);
            }
            Key::Enter => {
                if current_attempt_len < 5 {
                    return;
                }
                if !WORDS.contains(&*self.current_attempt) {
                    return;
                }

                let mut new_attempts = (*self.attempts).clone();
                let new_attempt_row = (*self.current_attempt)
                    .clone()
                    .chars()
                    .zip((*self.answer).chars())
                    .map(|(att, ans)| {
                        let status = evaluate_status(ans, att, (*self.answer).clone());
                        AttemptField {
                            char_field: att,
                            status,
                        }
                    }).collect::<Vec<AttemptField>>();
                new_attempts.fields.push(new_attempt_row.clone());

                let mut new_keyboard = (*self.virtual_keyboard).clone();
                for key in new_keyboard.iter_mut() {
                    for attempt_field in new_attempt_row.iter() {
                        if key.key.cmp_char(&attempt_field.char_field) {
                            if key.status.is_none() {
                                key.status = Some(attempt_field.status.clone());
                            } else if key.status == Some(Status::Almost) && attempt_field.status == Status::Found {
                                key.status = Some(Status::Found);
                            }
                        }
                    }
                }
                self.virtual_keyboard.set(new_keyboard);

                self.attempts.set(new_attempts);
                *self.attempt_index.borrow_mut() += 1;

                if (*self.current_attempt) == (*self.answer) {
                    self.result.set(Some(GameResult::Win));
                } else if *self.attempt_index.borrow() == MAX_ATTEMPTS {
                    self.result.set(Some(GameResult::Fail));
                }

                self.current_attempt.set(String::default());
            }
            _ => {}
        }
    }
}

#[hook]
pub fn use_board() -> UseBoardHandle {
    let current_attempt = use_state(String::new);
    let attempts: UseStateHandle<Attempts> = use_state(Attempts::new);
    let attempt_index = use_mut_ref(|| 0usize);
    let answer = use_memo(|_| get_word_of_the_day(), None::<()>);
    let virtual_keyboard = use_state(|| new_empty_virtual_keyboard().into());
    let result = use_state(|| None::<GameResult>);

    UseBoardHandle {
        current_attempt,
        attempts,
        attempt_index,
        virtual_keyboard,
        result,
        answer,
    }
}
