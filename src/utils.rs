use crate::consts::{KeyboardKeyType, Status, VirtualKey};

pub fn new_empty_virtual_keyboard() -> [VirtualKey; 29] {
    [
        VirtualKey::from_charkey('Q'),
        VirtualKey::from_charkey('W'),
        VirtualKey::from_charkey('E'),
        VirtualKey::from_charkey('R'),
        VirtualKey::from_charkey('T'),
        VirtualKey::from_charkey('Y'),
        VirtualKey::from_charkey('U'),
        VirtualKey::from_charkey('I'),
        VirtualKey::from_charkey('O'),
        VirtualKey::from_charkey('P'),
        VirtualKey::from_charkey('A'),
        VirtualKey::from_charkey('S'),
        VirtualKey::from_charkey('D'),
        VirtualKey::from_charkey('F'),
        VirtualKey::from_charkey('G'),
        VirtualKey::from_charkey('H'),
        VirtualKey::from_charkey('J'),
        VirtualKey::from_charkey('K'),
        VirtualKey::from_charkey('L'),
        VirtualKey::from_charkey('Ñ'),
        VirtualKey::from_key(KeyboardKeyType::Backspace),
        VirtualKey::from_charkey('Z'),
        VirtualKey::from_charkey('X'),
        VirtualKey::from_charkey('C'),
        VirtualKey::from_charkey('V'),
        VirtualKey::from_charkey('B'),
        VirtualKey::from_charkey('N'),
        VirtualKey::from_charkey('M'),
        VirtualKey::from_key(KeyboardKeyType::Enter),
    ]
}

pub fn evaluate_status(ans: char, attr: char, answer: String) -> Status {
    if ans == attr {
        Status::Found
    } else if answer.contains(attr) {
        Status::Almost
    } else {
        Status::NotFound
    }
}
