pub const MAX_ATTEMPTS: usize = 6;

#[derive(Clone, PartialEq)]
pub enum KeyboardKeyType {
    Enter,
    Backspace,
    CharKey(char),
}

impl KeyboardKeyType {
    pub fn cmp_char(&self, char_to_compare: &char) -> bool {
        match self {
            KeyboardKeyType::CharKey(ch) => ch == char_to_compare,
            _ => false
        }
    }
}

#[derive(Clone, PartialEq)]
pub enum Status {
    NotFound,
    Found,
    Almost,
}

#[derive(Clone, PartialEq)]
pub struct AttemptField {
    pub char_field: char,
    pub status: Status,
}

#[derive(Clone, PartialEq)]
pub struct Attempts {
    pub fields: Vec<Vec<AttemptField>>,
}

impl Attempts {
    pub fn new() -> Self {
        Attempts {
            fields: vec![]
        }
    }
}

#[derive(Clone, PartialEq)]
pub struct VirtualKey {
    pub key: KeyboardKeyType,
    pub status: Option<Status>,
}

impl VirtualKey {
    pub fn from_charkey(charkey: char) -> Self {
        VirtualKey {
            key: KeyboardKeyType::CharKey(charkey),
            status: None,
        }
    }

    pub fn from_key(key: KeyboardKeyType) -> Self {
        VirtualKey { key, status: None }
    }
}

#[derive(Copy, Clone)]
pub enum Key {
    // 13
    Enter,
    // 8
    Backspace,
    CharKey(char),
    Ignored,
}

impl From<u32> for Key {
    fn from(value: u32) -> Self {
        match value {
            8 => Key::Backspace,
            13 => Key::Enter,
            65..=90 => Key::CharKey(value as u8 as char),
            59 => Key::CharKey(209u8 as char),
            _ => Key::Ignored,
        }
    }
}

#[derive(PartialEq, Copy, Clone)]
pub enum GameResult {
    Win,
    Fail,
}
