use crate::consts::MAX_ATTEMPTS;
use yew::prelude::*;
use crate::consts::{Attempts, Status};

#[derive(Properties, PartialEq)]
pub struct ResultBoardProps {
    pub attempts: Attempts,
}

#[function_component]
pub fn ResultBoard(props: &ResultBoardProps) -> Html {
    let ResultBoardProps {
        attempts,
    } = props;

    html! {
        <div class="result-board">
            <p>
                { format!("Palabra encontrada en {}/{} intentos", attempts.fields.len(), MAX_ATTEMPTS) }
            </p><br/>
            <p> {
            for attempts.fields.iter().map(|attempt| {
                html_nested! {
                    <> {
                        for attempt.iter().map(| att| {
                            match att.status {
                                Status::Found => html_nested! { <> { "🟩" } </> },
                                Status::Almost => html_nested! { <>{ "🟨" }</> },
                                Status::NotFound => html_nested! { <>{ "⬛" }</> }

                            }
                        })
                    } <br/></>
                }
            })
            } </p>
        </div>
    }
}
