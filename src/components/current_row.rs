use yew::prelude::*;

#[derive(Properties, PartialEq)]
pub struct CurrentRowProps {
    pub text: AttrValue,
}

#[function_component]
pub fn CurrentRow(props: &CurrentRowProps) -> Html {
    let attempt = {
        let mut ret = [' '; 5];
        props
            .text
            .chars()
            .zip(ret.iter_mut())
            .for_each(|(val, ptr)| *ptr = val);
        ret
    };

    html! {
        { for attempt.map(|c| html! { <p>{ c }</p> }) }
    }
}
