use yew::prelude::*;

#[function_component]
pub fn EmptyRow() -> Html {
    html! {
        { for (0..5u8).map(|_| html! { <p></p> }) }
    }
}
