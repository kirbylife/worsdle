use crate::consts::{AttemptField, Status};
use yew::prelude::*;

#[derive(Properties, PartialEq)]
pub struct AttemptRowProps {
    pub attempt: Vec<AttemptField>,
}

#[function_component]
pub fn AttemptRow(props: &AttemptRowProps) -> Html {
    let AttemptRowProps { attempt } = props;

    html! {
        { for attempt.iter().map(|att| html_nested! {
            <p
                class={
                    match att.status {
                        Status::Found => "correct",
                        Status::Almost => "almost",
                        Status::NotFound => "missed",
                    }
                }
            >{ att.char_field }</p>
        })}
    }
}
