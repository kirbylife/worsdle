pub use crate::components::attempt_row::AttemptRow;
pub use crate::components::current_row::CurrentRow;
pub use crate::components::empty_row::EmptyRow;
pub use crate::components::keyboard::Keyboard;
pub use crate::components::result_board::ResultBoard;
pub use crate::components::board::Board;

mod board;
mod attempt_row;
mod current_row;
mod empty_row;
mod keyboard;
mod result_board;
