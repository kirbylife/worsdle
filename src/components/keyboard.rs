use crate::consts::{Key, KeyboardKeyType, VirtualKey, Status};
use yew::prelude::*;

#[derive(Properties, PartialEq)]
pub struct KeyboardProps {
    pub onclick: Callback<Key>,
    pub keyboard: UseStateHandle<Vec<VirtualKey>>,
}

#[function_component]
pub fn Keyboard(props: &KeyboardProps) -> Html {
    let handle_click = |k: Key| {
        let onclick = props.onclick.clone();

        Callback::from(move |_| {
            onclick.emit(k);
        }).clone()
    };

    html! {
        <div class="keyboard"> {
            for (props.keyboard).iter().map(|vk: &VirtualKey| {
                match vk.key {
                    KeyboardKeyType::CharKey(c) => html! {
                        <button
                            class={
                                match vk.status {
                                    Some(Status::Found) => "correct",
                                    Some(Status::NotFound) => "missed",
                                    Some(Status::Almost) => "almost",
                                    None => ""
                                }
                            }
                            onclick={ handle_click(Key::CharKey(c)) }
                        > { c } </button>
                    },
                    KeyboardKeyType::Backspace => html! { <button onclick={ handle_click(Key::Backspace) } class="key-big"><i class="las la-undo"></i></button> },
                    KeyboardKeyType::Enter => html! { <button onclick={ handle_click(Key::Enter) }  class="key-big"> { "Enviar" } </button> }
                }
            })
        } </div>
    }
}
