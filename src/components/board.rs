use yew::prelude::*;
use crate::components::{EmptyRow, CurrentRow, AttemptRow};
use crate::consts::{Attempts, MAX_ATTEMPTS};
use std::cmp::Ordering;

#[derive(Properties, PartialEq)]
pub struct BoardProps {
    pub attempts: Attempts,
    pub attempt_index: usize,
    pub current_attempt: AttrValue,
    pub onkeypress: Callback<KeyboardEvent>,
}

#[function_component]
pub fn Board(props: &BoardProps) -> Html {
    let BoardProps {
        attempts,
        attempt_index,
        current_attempt,
        onkeypress
    } = props;

    html! {
        <div class="board" onkeyup={onkeypress} tabindex="0">
            {
                for (0..MAX_ATTEMPTS).map(|i| {
                    match i.cmp(attempt_index) {
                        Ordering::Less => html! { <AttemptRow attempt={ attempts.fields[i].clone() } /> },
                        Ordering::Equal => html! { <CurrentRow text={ current_attempt } /> },
                        Ordering::Greater => html! { <EmptyRow /> }
                    }
                })
            }
        </div>
    }
}
