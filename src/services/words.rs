use chrono::Datelike;
use lazy_static::lazy_static;

lazy_static! {
    pub static ref WORDS: Vec<String> = include_str!("../../words.txt")
        .trim()
        .lines()
        .map(|w| w.trim().into())
        .collect();
}

pub fn get_word_of_the_day() -> String {
    let date = chrono::Utc::now();
    let year = date.year() as usize;
    let month = date.month() as usize;
    let day = date.day() as usize;
    let index = year + month + day;

    WORDS[index % WORDS.len()].clone()
}
