use yew::prelude::*;

use crate::components::Board;
use crate::components::Keyboard;
use crate::components::ResultBoard;
use crate::consts::{Key};
use crate::hooks::use_board;

#[cfg(debug_assertions)]
use gloo::console;

mod components;
mod consts;
mod hooks;
mod services;
mod utils;

#[function_component]
fn App() -> Html {
    let board = use_board();

    let onkeypress = {
        let board = board.clone();

        Callback::from(move |event: KeyboardEvent| {
            let key_code = event.key_code();
            board.send_key(Key::from(key_code));
            #[cfg(debug_assertions)]
            console::log!(&event);
        })
    };

    let onclick = {
        let board = board.clone();

        Callback::from(move |key| {
            board.send_key(key);
        })
    };

    html! {
        <>
            <header>
                <h1>{ "Worsdle" }</h1>
            </header>
            <main>
            <Board
                attempts={ (*board.attempts).clone() }
                attempt_index={ *board.attempt_index.borrow() }
                current_attempt={ (*board.current_attempt).clone() }
                onkeypress={ onkeypress }
            />
            <div>
                <span> {
                    match *board.result {
                        Some(_) => html! { <ResultBoard attempts={(*board.attempts).clone()} /> },
                        None => html! { <Keyboard
                                        keyboard={ (board.virtual_keyboard).clone() }
                                        onclick={ onclick } /> }
                    }
                } </span>
            </div>
            </main>
            <footer>
                <small> {
                    html_nested! {
                        <>
                        { "Hecho por " } <a href={ "https://kirbylife.dev" }> { "kirbylife" } </a>
                        </>
                    }
                } </small>
            </footer>
        </>
    }
}

fn main() {
    wasm_logger::init(wasm_logger::Config::default());
    yew::Renderer::<App>::new().render();
}
